from asyncio.windows_events import NULL
from email.mime import image
from tkinter import*
from tkinter import ttk
from turtle import clear, color
from PIL import Image,ImageTk 
from tkinter import messagebox

def dealerregister():
    selfroot=root
    selfroot.title=("Dealer Registration Window")
    selfroot.geometry("1500x800+0+0")
    selfroot.config(bg="white")
       #bg-image
    pathtophoto1=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\bg.jpg")
    selfbg=ImageTk.PhotoImage(pathtophoto1)
    panel1=Label(root,image=selfbg)
    panel1.image=selfbg
    panel1.place(x=250,y=0,relwidth=1,relheight=1)

        #left image
    pathtophotoleft=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\dealer.jpg")
    selfleft=ImageTk.PhotoImage(pathtophotoleft)
    panelleft=Label(root,image=selfleft)
    panelleft.image=selfleft
    panelleft.place(x=80,y=100,width=400,height=500)

        #regester frame
    frame1=Frame(selfroot,bg="white")
    frame1.place(x=380,y=100,width=700,height=500)

    title=Label(frame1,text="DEALER REGISTER FORM",font=("times new roman",20,"bold"),bg="white",fg="black").place(x=50,y=30)

    f_name=Label(frame1,text="First Name",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=80)
    txt_fname=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=100,width=200)

    l_name=Label(frame1,text="Last Name",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=80)
    txt_lname=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=100,width=200)


        #-----------------------------------------------
    contact=Label(frame1,text="Phone No.",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=130)
    txt_contact=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=150,width=200)

    email=Label(frame1,text="Email",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=130)
    txt_email=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=150,width=200)

        #-----------------------------------------------
    question=Label(frame1,text="Nature of material",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=180)

    cmb_quest=ttk.Combobox(frame1,font=("times new roman",10),state='readonly',justify=CENTER)
    cmb_quest['values']=("Select","Metal","Ceramic","Sand","Goods","Other")
    cmb_quest.place(x=50,y=200,width=200)
    cmb_quest.current(0)

        #------------------------------------------------
    quant=Label(frame1,text="Quantity",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=230)
    txt_quant=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=250,width=200)

    cands=Label(frame1,text="City & State",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=230)
    txt_cands=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=250,width=200)


    password=Label(frame1,text="Password",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=280)
    txt_password=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=300,width=200)
    cpassword=Label(frame1,text="Confirm Password",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=280)
    txt_cpassword=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=300,width=200)


        #--------terms
    chk=Checkbutton(frame1,text="I Agree to the Terms and Conditions",onvalue=1,offvalue=0,bg="white",font=("times new roman",12)).place(x=50,y=340)

    pathtophotobtnreg=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\rg.png")
    selfbtn_img=ImageTk.PhotoImage(pathtophotobtnreg)
    panelreg=Label(root,image=selfbtn_img)
    panelreg.image=selfbtn_img
    panelreg.place(x=430,y=520,width=170,height=35)

    answer=Label(frame1,text="Weight(in Kgs.)",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=180)
    txt_answer=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=200,width=200)



def dealerloginpage():
    selfroot = root
    selfroot.title=("Dealer Login")
    selfroot.geometry("1500x800+0+0")
    selfroot.config(bg="skyblue")

    #background-image
    pathtophoto1=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\bg.jpg")
    selfbg=ImageTk.PhotoImage(pathtophoto1)
    panel1=Label(root,image=selfbg)
    panel1.image=selfbg
    panel1.place(x=0,y=0,relwidth=1,relheight=1)


    frame=Frame(selfroot,bg="white")
    frame.place(x=610,y=170,width=340,height=450)

    img1=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\slogin.png")
    img1=img1.resize((100,100),Image.ANTIALIAS)
    selfimg1=ImageTk.PhotoImage(img1)
    lblimg1=Label(root,image=selfimg1,bg="white")
    lblimg1.image=selfimg1
    lblimg1.place(x=730,y=170,width=100,height=100)

    get_str=Label(frame,text="DEALER",font=("times new roman",20,"bold"),fg="black",bg="skyblue")
    get_str.place(x=120,y=100)


    #label
    username=lbl=Label(frame,text="Username",font=("times new roman",15,"bold"),fg="black",bg="white")
    username.place(x=65,y=150)

    selftxtuser=ttk.Entry(frame,font=("times new roman",15,"bold"))
    selftxtuser.place(x=40,y=180,width=270)

    username=lbl=Label(frame,text="Password",font=("times new roman",15,"bold"),fg="black",bg="white")
    username.place(x=70,y=225)

    selftxtpass=ttk.Entry(frame,font=("times new roman",15,"bold"))
    selftxtpass.place(x=40,y=250,width=270)

        #=========Icon images================


    img2=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\l.png")
    img2=img2.resize((25,25),Image.ANTIALIAS)
    selfimg2=ImageTk.PhotoImage(img2)
    lblimg2=Label(root,image=selfimg2,bg="white")
    lblimg2.image=selfimg2
    lblimg2.place(x=650,y=323,width=25,height=25)



    img3=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\11.png")
    img3=img3.resize((25,25),Image.ANTIALIAS)
    selfimg3=ImageTk.PhotoImage(img3)
    lblimg3=Label(root,image=selfimg3,bg="white")
    lblimg3.image=selfimg3
    lblimg3.place(x=650,y=393,width=25,height=25)


        #loginbutton
    loginbtn=Button(frame,text="Login",font=("times new roman",15,"bold"),bd=3,relief=RIDGE,fg="white",bg="black",activeforeground="white",activebackground="black")
    loginbtn.place(x=110,y=300,width=120,height=35)

        #regbutton
    registerbtn=Button(frame,command=dealerregister,text="New User Register",font=("times new roman",10,"bold"),borderwidth=0,fg="black",bg="white",activeforeground="black",activebackground="white")
    registerbtn.place(x=15,y=350,width=160)

        #forgotpasswordbutton
    registerbtn=Button(frame,text="Forgot Password",font=("times new roman",10,"bold"),borderwidth=0,fg="black",bg="white",activeforeground="black",activebackground="white")
    registerbtn.place(x=10,y=370,width=160)


def driverregister():
    
    selfroot=root
    selfroot.title=("Dealer Registration Window")
    selfroot.geometry("1500x800+0+0")
    selfroot.config(bg="white")
       #bg-image
    pathtophoto1=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\bg.jpg")
    selfbg=ImageTk.PhotoImage(pathtophoto1)
    panel1=Label(root,image=selfbg)
    panel1.image=selfbg
    panel1.place(x=250,y=0,relwidth=1,relheight=1)

        #left image
    pathtophotoleft=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\dealer.jpg")
    selfleft=ImageTk.PhotoImage(pathtophotoleft)
    panelleft=Label(root,image=selfleft)
    panelleft.image=selfleft
    panelleft.place(x=80,y=100,width=400,height=500)

        #regester frame
    frame1=Frame(selfroot,bg="white")
    frame1.place(x=380,y=100,width=700,height=500)

    title=Label(frame1,text="DEALER REGISTER FORM",font=("times new roman",20,"bold"),bg="white",fg="black").place(x=50,y=30)

    f_name=Label(frame1,text="Name",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=80)
    txt_fname=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=100,width=200)

    age=Label(frame1,text="Age",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=80)
    txt_age=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=100,width=200)


        #-----------------------------------------------
    contact=Label(frame1,text="Phone No.",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=130)
    txt_contact=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=150,width=200)

    email=Label(frame1,text="Email",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=130)
    txt_email=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=150,width=200)

        #-----------------------------------------------
    t_no=Label(frame1,text="Truck N",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=180)
    txt_t_no=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=200,width=200)


        #------------------------------------------------
    t_capacity=Label(frame1,text="Truck Capacity",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=230)
    txt_t_capacity=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=250,width=200)

    transporter_name=Label(frame1,text="Transporter Name",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=180)
    txt_transporter_name=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=200,width=200)

    exp=Label(frame1,text="Driving Experience",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=280)
    txt_exp=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=300,width=200)

    r1=Label(frame1,text="Interested Route-1",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=280)
    txt_r1=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=300,width=200)

    r2=Label(frame1,text="Interested Route-2",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=330)
    txt_r2=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=350,width=200)

    r3=Label(frame1,text="Interested Route-3",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=380)
    txt_r3=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=400,width=200)

    password=Label(frame1,text="Password",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=50,y=330)
    txt_password=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=50,y=350,width=200)
    cpassword=Label(frame1,text="Confirm Password",font=("times new roman",10,"bold"),bg="white",fg="black").place(x=370,y=230)
    txt_cpassword=Entry(frame1,font=("times new roman",10),bg="lightgray").place(x=370,y=250,width=200)


        #--------terms
    chk=Checkbutton(frame1,text="I Agree to the Terms and Conditions",onvalue=1,offvalue=0,bg="white",font=("times new roman",12)).place(x=50,y=390)

    pathtophotobtnreg=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\rg.png")
    selfbtn_img=ImageTk.PhotoImage(pathtophotobtnreg)
    panelreg=Label(root,image=selfbtn_img)
    panelreg.image=selfbtn_img
    panelreg.place(x=430,y=540,width=170,height=35)




def driverloginpage():
    selfroot = root
    selfroot.title=("Driver Login")
    selfroot.geometry("1500x800+0+0")
    selfroot.config(bg="skyblue")

    #background-image
    pathtophoto1=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\bg.jpg")
    selfbg=ImageTk.PhotoImage(pathtophoto1)
    panel1=Label(root,image=selfbg)
    panel1.image=selfbg
    panel1.place(x=0,y=0,relwidth=1,relheight=1)


    frame=Frame(selfroot,bg="white")
    frame.place(x=610,y=170,width=340,height=450)

    img1=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\slogin.png")
    img1=img1.resize((100,100),Image.ANTIALIAS)
    selfimg1=ImageTk.PhotoImage(img1)
    lblimg1=Label(root,image=selfimg1,bg="white")
    lblimg1.image=selfimg1
    lblimg1.place(x=730,y=170,width=100,height=100)

    get_str=Label(frame,text="DRIVER",font=("times new roman",20,"bold"),fg="black",bg="skyblue")
    get_str.place(x=120,y=100)


    #label
    username=lbl=Label(frame,text="Username",font=("times new roman",15,"bold"),fg="black",bg="white")
    username.place(x=65,y=150)

    selftxtuser=ttk.Entry(frame,font=("times new roman",15,"bold"))
    selftxtuser.place(x=40,y=180,width=270)

    username=lbl=Label(frame,text="Password",font=("times new roman",15,"bold"),fg="black",bg="white")
    username.place(x=70,y=225)

    selftxtpass=ttk.Entry(frame,font=("times new roman",15,"bold"))
    selftxtpass.place(x=40,y=250,width=270)

        #=========Icon images================


    img2=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\l.png")
    img2=img2.resize((25,25),Image.ANTIALIAS)
    selfimg2=ImageTk.PhotoImage(img2)
    lblimg2=Label(root,image=selfimg2,bg="white")
    lblimg2.image=selfimg2
    lblimg2.place(x=650,y=323,width=25,height=25)



    img3=Image.open(r"C:\Users\hp\OneDrive\Desktop\application\images\11.png")
    img3=img3.resize((25,25),Image.ANTIALIAS)
    selfimg3=ImageTk.PhotoImage(img3)
    lblimg3=Label(root,image=selfimg3,bg="white")
    lblimg3.image=selfimg3
    lblimg3.place(x=650,y=393,width=25,height=25)


        #loginbutton
    loginbtn=Button(frame,text="Login",font=("times new roman",15,"bold"),bd=3,relief=RIDGE,fg="white",bg="black",activeforeground="white",activebackground="black")
    loginbtn.place(x=110,y=300,width=120,height=35)

        #regbutton
    registerbtn=Button(frame,command=driverregister,text="New User Register",font=("times new roman",10,"bold"),borderwidth=0,fg="black",bg="white",activeforeground="black",activebackground="white")
    registerbtn.place(x=15,y=350,width=160)

        #forgotpasswordbutton
    registerbtn=Button(frame,text="Forgot Password",font=("times new roman",10,"bold"),borderwidth=0,fg="black",bg="white",activeforeground="black",activebackground="white")
    registerbtn.place(x=10,y=370,width=160)


class mainpage:
    def __init__(self,root):
        self.root = root
        self.root.title=("Main page")
        self.root.geometry("1500x800+0+0")
        self.root.config(bg="skyblue")
        headline=lbl=Label(self.root,text="SL DEALERS",font=("roman",45,"bold"),fg="white",bg="skyblue")
        headline.place(x=0,y=0)
        headline1=lbl=Label(self.root,text="Login as a Dealer or a Driver",font=("times new roman",55,"bold"),fg="green",bg="skyblue")
        headline1.place(x=305,y=180)
        dealerbtn=Button(self.root,command=dealerloginpage,text="DEALER",font=("times new roman",30,"bold"),bd=3,relief=RIDGE,fg="white",bg="black",activeforeground="white",activebackground="black")
        dealerbtn.place(x=550,y=300,width=400,height=75)
        driverbtn=Button(self.root,command=driverloginpage,text="DRIVER",font=("times new roman",30,"bold"),bd=3,relief=RIDGE,fg="white",bg="black",activeforeground="white",activebackground="black")
        driverbtn.place(x=550,y=400,width=400,height=75)




if __name__ == "__main__":
    root = Tk()
    app=mainpage(root)
    root.mainloop()
